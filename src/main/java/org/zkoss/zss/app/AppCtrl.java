package org.zkoss.zss.app;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import org.zkoss.zul.*;
import org.zkoss.zss.ui.*;
import org.zkoss.zss.ui.event.*;
import org.zkoss.zss.api.*;
import org.zkoss.zss.api.Range.*;
import org.zkoss.zss.api.model.*;

import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.SerializableEventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zss.ui.impl.DefaultUserActionManagerCtrl;

import org.zkoss.util.media.AMedia;

public class AppCtrl extends SelectorComposer<Component>{
	private class StackItem {
		String cmd;
		ArrayList<CellInfo> array;

		public StackItem(String cmd, ArrayList<CellInfo> array) {
			this.cmd = cmd;
			this.array = array;
		}
	}

	private class CellInfo {
		String ref;
		String val;

		public CellInfo(String ref, String val) {
			this.ref = ref;
			this.val = val;
		}
	}
	
	private static final long serialVersionUID = 1L;
	
	private Stack<StackItem> history = new Stack<StackItem>(), redo_history = new Stack<StackItem>();
	private ArrayList<CellInfo> temp_array = new ArrayList<CellInfo>();
	private int change_count = 0;
	private boolean history_reset = false;
	private Connection conn = null;
	
	@Wire
	private Listbox eventFilterList;
	
	@Wire
	private Grid infoList;
	
	@Wire
	private Spreadsheet ss;
	
	static private final Map<String,Book> sharedBook = new HashMap<String,Book>();
	
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		
		Book book = getSharedBook("blank.xlsx");
		ss.setBook(book);
		
		initDatabase();
		
		UserActionManager actionManager = ss.getUserActionManager();
		
        actionManager.registerHandler(
            DefaultUserActionManagerCtrl.Category.AUXACTION.getName(),
            AuxAction.SAVE_BOOK.getAction(), new UserActionHandler() {
					
			@Override
			public boolean process(UserActionContext ctx) {
				doSaveBook();
				return true;
			}
					
			@Override
			public boolean isEnabled(Book book, Sheet sheet) {
				return true;
			}
		});
        
        ss.addEventListener(Events.ON_AFTER_CELL_CHANGE, new SerializableEventListener<Event>() {
			private static final long serialVersionUID = 1L;
	
			@Override
			public void onEvent(Event event) throws Exception {
				new Thread() {
	    			@Override
	    			public void run() {
	    				try {
	    					save(getSharedBook("blank.xlsx"));
	    				} catch (IOException e) {
	    					e.printStackTrace();
	    				}
	    			}
	    		}.start();
			}
		});
	}
	
	private void initDatabase() {
		try {
            //load driver and get a database connection
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/wordpress?user=root&password=godh*7014ms");
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM wordpress.test");
            
            ResultSet rs = stmt.executeQuery();
            int index = 1;
            
            while (rs.next()) {
                Ranges.range(ss.getSelectedSheet(), 0, index).setCellValue(rs.getString(1));
                Ranges.range(ss.getSelectedSheet(), 1, index).setCellValue(rs.getInt(2));
                Ranges.range(ss.getSelectedSheet(), 2, index).setCellValue(rs.getInt(3));
                Ranges.range(ss.getSelectedSheet(), 3, index).setCellValue(rs.getInt(4));
                
                change_count += 4;
                index++;
            }
            
            stmt.close();
        } catch(Exception e){
            e.printStackTrace();
        }
	}
		
	private Book getSharedBook(String bookname){
		Book book;
		synchronized (sharedBook){
			book = sharedBook.get(bookname);
			if(book==null){
				book = importBook(bookname);
				book.setShareScope("application");
				sharedBook.put(bookname, book);
			}
		}
		return book;
	}
	
	private Book importBook(String bookname){
		Importer imp = Importers.getImporter();
		try {
			Book book = imp.imports(WebApps.getCurrent().getResource("/WEB-INF/books/" + bookname),	bookname);
			return book;
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(),e);
		}
	}
	
	private void doSaveBook(){
		try {
			Exporter exporter = Exporters.getExporter();
			Book book = ss.getBook();
			File file = File.createTempFile(Long.toString(System.currentTimeMillis()),"temp");
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(file);
				exporter.export(book, fos);
			}finally{
				if(fos!=null){
					fos.close();
				}
			}

			Filedownload.save(new AMedia("blank", "xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", file, true));
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void save(Book book) throws IOException{
		String savingPath = WebApps.getCurrent().getRealPath("/WEB-INF/books/")+File.separator;
		File targetFile = new File(savingPath
				+ book.getBookName());
		FileOutputStream fos = null;
		try{
			//write to temporary file first to avoid write error damage original file 
			File temp = File.createTempFile("temp", targetFile.getName());
			fos = new FileOutputStream(temp);
			Exporters.getExporter().export(book, fos);
			
			fos.close();
			fos = null;
			
			copy(temp,targetFile);
			temp.delete();
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(fos!=null)
				fos.close();
		}
	}
	
	public void copy(File src, File dest) throws IOException {
		FileInputStream fis = null;
		FileOutputStream fos = null;
		byte[] buff = new byte[1024];
		try{
			fis = new FileInputStream(src);
			fos = new FileOutputStream(dest);
			int r;
			while( (r=fis.read(buff))>-1){
				fos.write(buff,0,r);
			}
		}finally{
			if(fis!=null){
				try{
					fis.close();
				}catch(Exception x){}
			}
			if(fos!=null){
				try{
					fos.close();
				}catch(Exception x){}
			}
		}
	}
	
	public void setValue(ArrayList<CellInfo> array) {
		for(int i=0; i < array.size(); i++) {
			Ranges.range(ss.getSelectedSheet(), array.get(i).ref).setCellValue(array.get(i).val);
			change_count++;
		}
		
		if(array.size() == 1) {
			ss.focusTo(Ranges.range(ss.getSelectedSheet(), array.get(0).ref).getRow(), Ranges.range(ss.getSelectedSheet(), array.get(0).ref).getColumn());
		}
	}
	
	public void delValue(ArrayList<CellInfo> array) {
		for(int i=0; i < array.size(); i++) {
			Ranges.range(ss.getSelectedSheet(), array.get(i).ref).clearContents();
			change_count++;
		}
		
		ss.focusTo(Ranges.range(ss.getSelectedSheet(), array.get(0).ref).getRow(), Ranges.range(ss.getSelectedSheet(), array.get(0).ref).getColumn());
	}
	
	private void storeSelectedValue(int row, int lastRow, int column, int lastColumn) {
		for (int i = row; i <= lastRow; i++) {
			for (int j = column; j <= lastColumn; j++) {
				String ref = Ranges.getCellRefString(i, j);
				String cellText = Ranges.range(ss.getSelectedSheet(), i, j).getCellEditText();

				if (cellText.length() > 0) {
					temp_array.add(new CellInfo(ref, cellText));
				}
			}
		}
	}
	
	//Events
	@Listen("onPlus = #plus")
	public void onPlus(Event event) throws Exception {
		AreaRef ref = ss.getSelection();
		Sheet sheet = ss.getSelectedSheet();

		if (ref.getRow() == 0 && ref.getLastRow() == 1048575) {
			Ranges.range(sheet, 0, ref.getColumn(), 1048575, ref.getLastColumn()).insert(InsertShift.RIGHT,
					InsertCopyOrigin.FORMAT_LEFT_ABOVE);
		}

		else if (ref.getColumn() == 0 && ref.getLastColumn() == 16383) {
			Ranges.range(sheet, ref.getRow(), 0, ref.getLastRow(), 16383).insert(InsertShift.DOWN,
					InsertCopyOrigin.FORMAT_LEFT_ABOVE);
		}
	}

	@Listen("onMinus = #minus")
	public void onMinus(Event event) {
		AreaRef ref = ss.getSelection();
		Sheet sheet = ss.getSelectedSheet();

		if (ref.getRow() == 0 && ref.getLastRow() == 1048575) {
			Ranges.range(sheet, 0, ref.getColumn(), 1048575, ref.getLastColumn()).delete(DeleteShift.LEFT);
		}

		else if (ref.getColumn() == 0 && ref.getLastColumn() == 16383) {
			Ranges.range(sheet, ref.getRow(), 0, ref.getLastRow(), 16383).delete(DeleteShift.UP);
		}
	}

	@Listen("onCtrlKey = #ss")
	public void onCtrlKey(KeyEvent event) {
		int key = event.getKeyCode();
		Sheet sheet = ss.getSelectedSheet();
		int row = ss.getCellFocus().getRow();
		int col = ss.getCellFocus().getColumn();

		boolean flag = false;
		int cal_val = -1;

		flag = Ranges.range(sheet, row, col).getCellData().isBlank();

		if (flag) {
			cal_val = 0;
		}

		//////////////////////////////////////////////////////////////////////

		if (key == 65) { // select all
			AreaRef ref = new AreaRef(0, 0, ss.getMaxVisibleRows(), ss.getMaxVisibleColumns());
			ss.setSelection(ref);
			storeSelectedValue(0, 999, 0, 25);
		}

		else if (key == 82) { // redo
			if (redo_history.size() > 0) {
				StackItem item = redo_history.pop();
				history.push(item);
				history_reset = true;

				if (item.cmd.equals("del_val")) {
					delValue(item.array);
				}
				if (item.cmd.equals("val")) {
					setValue(item.array);
				}
			}
		}

		else if (key == 90) { // undo
			if (history.size() > 0) {
				StackItem item = history.pop();
				redo_history.push(item);

				if (item.cmd.equals("val")) {
					delValue(item.array);
				}
				if (item.cmd.equals("del_val")) {
					setValue(item.array);
				}
			}
		}

		else if (key == 83) { // save
			doSaveBook();
		}

		else if (key == 40) { // down
			for (int i = row; i < ss.getMaxVisibleRows(); i++) {
				if (Ranges.range(sheet, i, col).getCellData().isBlank() != flag) {
					if (!flag && i == row) {
						flag = !flag;
						cal_val = 0;
						i = row - 1;
						continue;
					}

					if (!event.isShiftKey()) {
						ss.focusTo(i + cal_val, col);
					} else {
						int lastRow = i + cal_val;

						if (lastRow < ss.getSelection().getLastRow() - 1) {
							lastRow = ss.getSelection().getLastRow() - 1;
						}

						AreaRef ref = new AreaRef(ss.getSelection().getRow(), ss.getSelection().getColumn(), lastRow,
								ss.getSelection().getLastColumn());
						ss.focusTo(lastRow, col);
						ss.setSelection(ref);
					}

					return;
				}
			}

			if (!event.isShiftKey()) {
				ss.focusTo(ss.getMaxVisibleRows() - 1, col);
			} else {
				AreaRef ref = new AreaRef(ss.getSelection().getRow(), ss.getSelection().getColumn(),
						ss.getMaxVisibleRows() - 1, ss.getSelection().getLastColumn());
				ss.focusTo(ss.getMaxVisibleRows() - 1, col);
				ss.setSelection(ref);
			}
		}

		else if (key == 39) { // right
			for (int i = col; i < ss.getMaxVisibleColumns(); i++) {
				if (Ranges.range(sheet, row, i).getCellData().isBlank() != flag) {
					if (!flag && i == col) {
						flag = !flag;
						cal_val = 0;
						i = col - 1;
						continue;
					}

					if (!event.isShiftKey()) {
						ss.focusTo(row, i + cal_val);
					} else {
						int lastColumn = i + cal_val;

						if (lastColumn < ss.getSelection().getLastColumn() - 1) {
							lastColumn = ss.getSelection().getLastColumn() - 1;
						}

						AreaRef ref = new AreaRef(ss.getSelection().getRow(), ss.getSelection().getColumn(),
								ss.getSelection().getLastRow(), lastColumn);
						ss.focusTo(row, lastColumn);
						ss.setSelection(ref);
					}

					return;
				}
			}

			if (!event.isShiftKey()) {
				ss.focusTo(row, ss.getMaxVisibleColumns() - 1);
			} else {
				AreaRef ref = new AreaRef(ss.getSelection().getRow(), ss.getSelection().getColumn(),
						ss.getSelection().getLastRow(), ss.getMaxVisibleColumns() - 1);
				ss.focusTo(row, ss.getMaxVisibleColumns() - 1);
				ss.setSelection(ref);
			}
		}

		else if (key == 38) { // up
			for (int i = row; i >= 0; i--) {
				if (Ranges.range(sheet, i, col).getCellData().isBlank() != flag) {
					if (!flag && i == row) {
						flag = !flag;
						cal_val = 0;
						i = row + 1;
						continue;
					}

					if (!event.isShiftKey()) {
						ss.focusTo(i - cal_val, col);
					} else {
						int firstRow = i - cal_val;

						if (firstRow > ss.getSelection().getRow() + 1) {
							firstRow = ss.getSelection().getRow() + 1;
						}

						AreaRef ref = new AreaRef(firstRow, ss.getSelection().getColumn(),
								ss.getSelection().getLastRow(), ss.getSelection().getLastColumn());
						ss.focusTo(firstRow, col);
						ss.setSelection(ref);
					}

					return;
				}
			}

			if (!event.isShiftKey()) {
				ss.focusTo(0, col);
			} else {
				AreaRef ref = new AreaRef(0, ss.getSelection().getColumn(), ss.getSelection().getLastRow(),
						ss.getSelection().getLastColumn());
				ss.focusTo(0, col);
				ss.setSelection(ref);
			}
		}

		else if (key == 37) { // left
			for (int i = col; i >= 0; i--) {
				if (Ranges.range(sheet, row, i).getCellData().isBlank() != flag) {
					if (!flag && i == col) {
						flag = !flag;
						cal_val = 0;
						i = col + 1;
						continue;
					}

					if (!event.isShiftKey()) {
						ss.focusTo(row, i - cal_val);
					} else {
						int firstColumn = i - cal_val;

						if (firstColumn > ss.getSelection().getColumn() + 1) {
							firstColumn = ss.getSelection().getColumn() + 1;
						}

						AreaRef ref = new AreaRef(ss.getSelection().getRow(), firstColumn,
								ss.getSelection().getLastRow(), ss.getSelection().getLastColumn());
						ss.focusTo(row, firstColumn);
						ss.setSelection(ref);
					}

					return;
				}
			}

			if (!event.isShiftKey()) {
				ss.focusTo(row, 0);
			} else {
				AreaRef ref = new AreaRef(ss.getSelection().getRow(), 0, ss.getSelection().getLastRow(),
						ss.getSelection().getLastColumn());
				ss.focusTo(row, 0);
				ss.setSelection(ref);
			}
		}
	}

	@Listen("onCellFocus = #ss")
	public void onCellFocus(CellEvent event) {
		if(temp_array.size() > 0) {
			temp_array = new ArrayList<CellInfo>();
		}
	}

	@Listen("onCellSelection = #ss")
	public void onCellSelection(CellSelectionEvent event) {
		storeSelectedValue(event.getRow(), event.getLastRow(), event.getColumn(), event.getLastColumn());
	}

	@Listen("onAfterCellChange = #ss")
	public void onAfterCellChange(CellAreaEvent event) {
		if (change_count == 0) {
			if (history_reset) {
				history_reset = false;
				history.clear();
				redo_history.clear();
			}
			
			ArrayList<CellInfo> array = new ArrayList<CellInfo>();

			String range = Ranges.getCellRefString(event.getRow(), event.getColumn());
			String val = Ranges.range(event.getSheet(), event.getArea()).getCellFormatText();

			array.add(new CellInfo(range, val));

			if (val.length() > 0) {
				history.push(new StackItem("val", array));
			}

			else {
				history.push(new StackItem("del_val", temp_array));
			}
			
			if(event.getRow() > 0 && event.getRow() <= 3 && event.getColumn() > 0 && event.getColumn() <= 8) {
				PreparedStatement stmt = null;
				
				try {
					Integer.parseInt(val); // check val is integer
					String command = "UPDATE wordpress.test SET " + "value" + event.getRow() + "=" + val + " where name = \""
							+ Ranges.range(event.getSheet(), 0, event.getColumn()).getCellFormatText() + "\""; 
					stmt = conn.prepareStatement(command);
			 
			        //execute the statement
			        System.out.println(stmt.toString());
			        stmt.executeUpdate();
				}
				catch(Exception e) {
					e.printStackTrace();
				}
				finally {
					if (stmt != null) {
		                try {
		                    stmt.close();
		                } catch (SQLException ex) {
		                    ex.printStackTrace();
		                }
		            }
				}
			}
		} else {
			change_count--;
		}
	}
}